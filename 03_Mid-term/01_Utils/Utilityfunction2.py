#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 16:16:42 2024
The purpose of the extract_column function is to extract a specific column 
from a list of dictionaries representing rows of data and 
return it as a list of values.

"""

def extract_column(data_rows, column_name):
    """
    Extracts a column from a list of dictionaries.

    Parameters
    ----------
    data_rows : list of dict
        A list of dictionaries representing rows of data.
    column_name : str
        The name of the column to extract.

    Returns
    -------
    list
        A list of values from the specified column. If a row does not contain the column key, None is substituted.
    """
    column = [row.get(column_name) for row in data_rows]
    return column
