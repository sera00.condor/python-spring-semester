import argparse
import csv
from pathlib import Path

# %%
# Define structure column
COLUMN_STRUCTURE = {
    "ID": {"type": "int", "mandatory": True},
    "surname": {"type": "str", "mandatory": True},
    "middle.name": {"type": "str", "mandatory": False},
    "first.name": {"type": "str", "mandatory": True},
    "Insurance": {"type": "str", "mandatory": False},
    "gender": {"type": "str", "mandatory": True},
    "Smoker": {"type": "bool", "mandatory": False},
    "Alcohol": {"type": "bool", "mandatory": False},
    "Family_History_HD": {"type": "bool", "mandatory": False},
    "systolic_bp": {"type": "int", "mandatory": True},
    "diastolic_bp": {"type": "int", "mandatory": True},
    "Age": {"type": "int", "mandatory": True},
    "height": {"type": "int", "mandatory": True},
    "weight": {"type": "int", "mandatory": True},
    "RandomBSL": {"type": "float", "mandatory": False},
    "Medication_Allergy_Code": {"type": "str", "mandatory": False},
    "HbA1c": {"type": "float", "mandatory": False},
    "DiabetesDiagnosis": {"type": "bool", "mandatory": False},
    "KidneyDisease": {"type": "bool", "mandatory": False},
}


# %%
def load_csv(file_path):
    """
    Function to load CSV data; Look at the helper function provided
    Load CSV data from the specified file and return it along with the reader object used to read the data.

    Parameters
    ----------
    file_path : str or Path
        The path to the CSV file.

    Returns
    -------
    csv.DictReader, list of dicts
        A tuple containing the reader object for the CSV file and a list of dictionaries, where each dictionary
        represents a row of data from the CSV file.
    """
    file_path = Path(file_path)
    if not file_path.exists() or not file_path.is_file():
        raise SystemExit(f"{str(file_path)!r} does not exist or is not a regular file")
    with open(file_path) as fp:
        reader = csv.DictReader(fp)
        data_rows = list(reader)
        return reader, data_rows


def extract_column(data_rows: list[dict], column_name: str):
    """
    Function to extract a column from data; Look at the helper function provided
    Extracts a column from a list of dictionaries.

    Parameters
    ----------
    data_rows : list of dict
        A list of dictionaries representing rows of data.
    column_name : str
        The name of the column to extract.

    Returns
    -------
    list
        A list of values from the specified column. If a row does not contain the column key, None is substituted.
    """
    column = [row.get(column_name) for row in data_rows]
    return column


def save_csv(data_rows: list[dict], file_path):
    """
    Function to save data to CSV (replace with appropriate logic); Look at the helper function provided
    Writes a list of row dictionaries to a CSV file specified by file_path.

    Each dictionary in data_rows represents a row of data, with keys as column names and values as corresponding
    data values. The function converts values to strings before writing to the file.

    If data_rows is empty, no file will be created or written.

    Parameters:
        data_rows (list of dict): A list of row dictionaries to be written to the CSV file.
        file_path (str or Path): The path to the output CSV file.

    Returns:
        None
    """
    file_path = Path(file_path)
    if not data_rows:
        return
    with open(file_path, "w") as fp:
        writer = csv.DictWriter(fp, list(data_rows[0].keys()))
        writer.writeheader()
        for data_row in data_rows:
            writer.writerow({k: str(v) for k, v in data_row.items()})


# %% Task 1: Data ingestion and validation
# For example:
# Function to check if data has n rows (replace with appropriate logic)
def has_n_rows(data_rows: list[dict], n):
    # Implement logic to check if the length of data_rows is n
    # (This functionality is not required for the assignment)
    return len(data_rows) == n


# Predicate : Verifying Required Columns:
def has_required_columns(csv_reader: csv.DictReader, column_names):
    # csv_reader object has an attribute fieldnames (which may be accessed as csv_reader.fieldnames)
    # which returns a list containing the names of the columns in the file as strings.
    return all(col in csv_reader.fieldnames for col in column_names)


# Function: Ensuring No Extra Columns:
def has_no_extra_columns(csv_reader: csv.DictReader, column_names):
    return all(col in column_names for col in csv_reader.fieldnames)


# Function: Calculating Empty Value Proportion
def proportion_empty(column: list):
    count_none = column.count(None)
    count_empty = column.count("")

    return (count_none + count_empty) / len(column)


# Function: Validating Data Rows
def is_valid(row: dict, column_definitions: dict):
    # Flag result
    result = False

    if row.keys() == column_definitions.keys():

        # Filter rules for columns based on the corresponding column column_definitions
        filter_column_structure_base_column_definition = {
            key: value
            for key, value in COLUMN_STRUCTURE.items()
            if key in column_definitions
        }

        # Check all columns are required to meet the requirements
        check_column_in_column_definition_is_mandatory = [
            (
                True
                if value["mandatory"] and row[key] != "NA" and row[key] != ""
                else True if not value["mandatory"] else False
            )
            for key, value in filter_column_structure_base_column_definition.items()
        ]

        # If the structure of all columns is correct and there are enough columns in column_definitions
        if all(check_column_in_column_definition_is_mandatory):
            result = True

    return result


# Function: Determining Valid Row Proportion
def proportion_valid(data_rows: list[dict], column_definitions: dict):
    # List receives bool value of each row in data_rows
    data_row_valid = [is_valid(row, column_definitions) for row in data_rows]

    # The value of the total number of valid lines
    sum_true_data_row_valid = sum(data_row_valid)

    return sum_true_data_row_valid / len(data_rows)


# %% Task 2: Data Correction and Analysis
# Function: clean individual dictionary records.
def preprocessing_phase1(data_rows: list[dict]):
    # Start Define child functions -------------------------------------------------------------
    # ------------------------------------------------------------------------------------------
    # Function clean each column match type data of column
    def clean_column(
        data_rows: list[dict], column_name: str, list_column_default: dict
    ):
        if list_column_default[column_name] == "int":
            for row in data_rows:
                if row[column_name] == "NA" or row[column_name] == "Not recorded":
                    row[column_name] = "NA"
                elif isinstance(row[column_name], str):
                    row[column_name] = int(float(row[column_name].strip()))
        elif list_column_default[column_name] == "float":
            for row in data_rows:
                if row[column_name] == "NA" or row[column_name] == "Not recorded":
                    row[column_name] = "NA"
                elif isinstance(row[column_name], str):
                    row[column_name] = float(row[column_name].strip())
        elif list_column_default[column_name] == "bool":
            for row in data_rows:
                if row[column_name] == "NA" or row[column_name] == "Not recorded":
                    row[column_name] = "NA"
                elif row[column_name] == "TRUE":
                    row[column_name] = True
                elif row[column_name] == "FALSE":
                    row[column_name] = False
        else:
            for row in data_rows:
                if row[column_name] == "NA":
                    row[column_name] = "NA"
                elif isinstance(row[column_name], str):
                    row[column_name] = row[column_name].strip()

    # End Define child functions -----------------------------------------------------------
    # --------------------------------------------------------------------------------------

    # Get column name
    column_name_list_type = {
        "ID": "int",
        "surname": "str",
        "middle.name": "str",
        "first.name": "str",
        "Insurance": "str",
        "gender": "str",
        "Smoker": "bool",
        "Alcohol": "bool",
        "Family_History_HD": "bool",
        "systolic_bp": "int",
        "diastolic_bp": "int",
        "Age": "int",
        "height": "int",
        "weight": "int",
        "RandomBSL": "float",
        "Medication_Allergy_Code": "str",
        "HbA1c": "float",
        "DiabetesDiagnosis": "bool",
        "KidneyDisease": "bool",
    }

    # Column name in data read from file
    column_name_lst = list(data_rows[0].keys())

    # The loop iterates through each column and cleans the corresponding column data
    for column in column_name_lst:
        clean_column(
            data_rows=data_rows,
            column_name=column,
            list_column_default=column_name_list_type,
        )


# Function: Summarise
def summarise_data(data_rows: list[dict]):
    # Start Define child functions ---------------------------------------------------------
    # --------------------------------------------------------------------------------------

    # Function count age range
    def age_count_range(a, b, ages_list):
        count_val = 0
        if b == "None":
            count_val = sum(1 for age in ages_list if age != "NA" and a <= age)
        else:
            count_val = sum(1 for age in ages_list if age != "NA" and a <= age <= b)

        return count_val

    # Funtions for child task - Age Group and Diagnosis
    # Filter age group and is DiabetesDiagnosis
    def filter_age_group_is_diagnosis(age1, age2):
        return list(
            filter(
                lambda data: data["Age"] != "NA"
                and age1 <= data["Age"] <= age2
                and data["DiabetesDiagnosis"],
                data_rows,
            )
        )

    # Filter age group and not is DiabetesDiagnosis
    def filter_age_group_not_diagnosis(age1, age2):
        return list(
            filter(
                lambda data: data["Age"] != "NA"
                and age1 <= data["Age"] <= age2
                and not data["DiabetesDiagnosis"],
                data_rows,
            )
        )

    # Template string print
    def age_group_diagnosis_str(
        age1,
        age2,
    ):
        str_val = ""
        if str_val == "None":
            str_val = (
                f"Age bracket {age1}-{age2}: previously diagnosed: {len(filter_age_group_is_diagnosis(age1, age2))}, "
                f"not previously diagnosed: {len(filter_age_group_not_diagnosis(age1, age2))}"
            )
        return

    # Extract data Undiagnosed Patients (Over 64):
    def filter_over64_undiagnosed_patients(age):
        return list(
            filter(
                lambda data: data["Age"] != "NA"
                and age <= data["Age"]
                and not data["DiabetesDiagnosis"],
                data_rows,
            )
        )

    # End Define child functions -----------------------------------------------------------
    # --------------------------------------------------------------------------------------

    preprocessing_phase1(data_rows)

    # Age Bracket Counts: --------------------------------
    # Extract data for age column
    column_age = extract_column(data_rows=data_rows, column_name="Age")

    # Print
    age_bracket_count_str = (
        f"Age brackets: 0-17: {age_count_range(0, 17, column_age)}, "
        f"18-24: {age_count_range(18, 24, column_age)}, "
        f"25-44: {age_count_range(25, 44, column_age)}, "
        f"45-64: {age_count_range(45, 64, column_age)}, "
        f"65-84: {age_count_range(65, 84, column_age)}, "
        f"85+: {age_count_range(85, 'None', column_age)}"
    )
    print(age_bracket_count_str)

    # Diabetes Diagnosis: ---------------------------------
    # Extract data for Diabetes Diagnosis column
    column_Diabetes_Diagnosis = extract_column(
        data_rows=data_rows, column_name="DiabetesDiagnosis"
    )

    # Count value
    count_previously_diagnosed = sum(
        1 for value in column_Diabetes_Diagnosis if value is True
    )

    count_not_previously_diagnosed = sum(
        1 for value in column_Diabetes_Diagnosis if value is False
    )

    # Calc percent by count value
    count_previously_diagnosed_percent = (
        count_previously_diagnosed
        / (count_previously_diagnosed + count_not_previously_diagnosed)
        * 100
    )

    count_not_previously_diagnosed_percent = 100 - count_previously_diagnosed_percent

    # Print string value and round percent to 2 decimal places
    diabetes_diagnosis_str = (
        f"Previously diagnosed: {count_previously_diagnosed} ({count_previously_diagnosed_percent:.2f}%),"
        f" not previously diagnosed: {count_not_previously_diagnosed} ({count_not_previously_diagnosed_percent:.2f}%)"
    )
    print(diabetes_diagnosis_str)

    # Age Group and Diagnosis: -----------------------------
    print(age_group_diagnosis_str(0, 17))
    print(age_group_diagnosis_str(18, 24))
    print(age_group_diagnosis_str(25, 64))
    print(age_group_diagnosis_str(65, 84))
    print(f"{age_group_diagnosis_str(85, 'None')}")

    # Undiagnosed Patients (Over 64): -----------------------
    # Filter data
    filter_data_ov64_un_patients = filter_over64_undiagnosed_patients(age=64)

    # Extract ID column from filter data
    extract_id_from_filter_data = extract_column(filter_data_ov64_un_patients, "ID")

    # Print
    print("Undiagnosed patients over 64:")
    for item in extract_id_from_filter_data:
        print(item)
    print("=====")


# %% Task: Blood Pressure Analysis
# Function calc MAP
def calc_map(systolic_bp: int, diastolic_bp: int):
    map_val = diastolic_bp + 0.33 * (systolic_bp - diastolic_bp)
    return map_val


# Funtion write MAP value to list dictionary data_rows
def write_with_map(data_rows: list[dict]):
    for row in data_rows:
        if row["systolic_bp"] != "NA" and row["diastolic_bp"] != "NA":
            row["MAP"] = calc_map(row["systolic_bp"], row["diastolic_bp"])


# Main function
def data_preprocessing(data_rows: list[dict]):
    """
    This function parses command line arguments to get the CSV file path,
    loads the CSV file using load_csv function, and prints the field names.
    """
    data_rows_clean: list[dict] = []

    parser = argparse.ArgumentParser()
    parser.add_argument("csv_file", metavar="CSV_FILE", type=Path)
    args = parser.parse_args()
    reader, data_rows = load_csv(args.csv_file)
    print(reader.fieldnames)

    return data_rows_clean


# Command-line interface (CLI)
# Implement command-line argument parsing to execute the main function with a specified CSV file.
if __name__ == "__main__":
    # Read csv
    file_path = "/02_Data/midData.csv"
    reader, data_row = load_csv(file_path=file_path)

    # Call data_preprocessing function
    data_preprocessing(data_rows=data_row)
