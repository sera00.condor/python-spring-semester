#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 16:10:54 2024
This function loads data from a CSV file and returns a reader object and a list of dictionaries.
"""
import csv
from pathlib import Path

def load_csv(file_path):
    """
    Load CSV data from the specified file and return it along with the reader object used to read the data.

    Parameters
    ----------
    file_path : str or Path
        The path to the CSV file.

    Returns
    -------
    csv.DictReader, list of dicts
        A tuple containing the reader object for the CSV file and a list of dictionaries, where each dictionary
        represents a row of data from the CSV file.
    """
    file_path = Path(file_path)
    if not file_path.exists() or not file_path.is_file():
        raise SystemExit(f'{str(file_path)!r} does not exist or is not a regular file')
    with open(file_path) as fp:
        reader = csv.DictReader(fp)
        data_rows = list(reader)
        return reader, data_rows