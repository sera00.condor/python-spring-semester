#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 13 16:18:57 2024

Purpose: Writes a list of dictionaries to a CSV file.
"""
import csv
from pathlib import Path

def save_csv(data_rows, file_path):
    """
    Writes a list of row dictionaries to a CSV file specified by file_path.

    Each dictionary in data_rows represents a row of data, with keys as column names and values as corresponding
    data values. The function converts values to strings before writing to the file.

    If data_rows is empty, no file will be created or written.

    Parameters:
        data_rows (list of dict): A list of row dictionaries to be written to the CSV file.
        file_path (str or Path): The path to the output CSV file.

    Returns:
        None
    """
    file_path = Path(file_path)
    if not data_rows:
        return
    with open(file_path, 'w') as fp:
        writer = csv.DictWriter(fp, list(data_rows[0].keys()))
        writer.writeheader()
        for data_row in data_rows:
            writer.writerow({k: str(v) for k, v in data_row.items()})