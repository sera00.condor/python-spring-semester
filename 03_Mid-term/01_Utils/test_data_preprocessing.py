#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@updated: mht, 13 Mar 2024

The file detected many errors:
    - Miss 1 variable in `assert is_valid(row) is expected`
"""

import csv
import re
import sys
import pytest


# %% Constants used in testing

DIABETES1_CSV = "midData.csv"

RECORDS = [
    {"name": "Fred", "age": "27"},
    {"name": "Mary", "age": "36"},
]
FIELDNAMES = list(RECORDS[0].keys())

RECORDS_PLUS = [
    {"name": "Fred", "age": "27", "atsi": "False"},
    {"name": "Mary", "age": "36", "atsi": "True"},
]
FIELDNAMES_PLUS = list(RECORDS_PLUS[0].keys())

VALID_ROW = {
    "ID": "500494",
    "surname": "PISCOPIO",
    "middle.name": "Shedrick",
    "first.name": "Paris",
    "gender": "Male",
    "Smoker": "FALSE",
    "systolic_bp": "106",
    "diastolic_bp": "92",
    "Age": "39",
    "height": "138.3",
    "weight": "59",
    "RandomBSL": " 6.0",
    "DiabetesDiagnosis": "FALSE",
}

VALID_ROW_OPTIONALS = {
    "ID": "500494",
    "surname": "PISCOPIO",
    "middle.name": "",
    "first.name": "Paris",
    "gender": "Male",
    "Smoker": "FALSE",
    "systolic_bp": "106",
    "diastolic_bp": "92",
    "Age": "39",
    "height": "138.3",
    "weight": "59",
    "RandomBSL": "",
    "DiabetesDiagnosis": "",
}

VALID_ROW_STRUCTURE = {
    "ID": "500494",
    "surname": "PISCOPIO",
    "middle.name": "Shedrick",
    "first.name": "Paris",
    "gender": "Male",
    "Smoker": "FALSE",
    "systolic_bp": "106",
    "diastolic_bp": "92",
    "Age": "39",
    "height": "138.3",
    "weight": "59",
    "RandomBSL": " 6.0",
    "DiabetesDiagnosis": "FALSE",
}

INVALID_ROW_SURNAME = {
    "ID": "553819",
    "surname": "",
    "middle.name": "",
    "first.name": "Khatanbaatar, Erik",
    "gender": "Male",
    "Smoker": "FALSE",
    "systolic_bp": "125",
    "diastolic_bp": "21",
    "Age": "49",
    "height": "183.7",
    "weight": "98",
    "RandomBSL": " 7.2",
    "DiabetesDiagnosis": "TRUE",
}

INVALID_ROW_WEIGHT = {
    "ID": "903107",
    "surname": "CASTILLO",
    "middle.name": "Joshua",
    "first.name": "Kya",
    "gender": "Male",
    "Smoker": "FALSE",
    "systolic_bp": "131",
    "diastolic_bp": "87",
    "Age": "81",
    "height": "178.6",
    "weight": "NA",
    "RandomBSL": " 3.4",
    "DiabetesDiagnosis": "FALSE",
}


# %% These are things used by the tests to generate test files


@pytest.fixture
def csv_file_factory(tmp_path):
    def _csv_file_factory(records):
        csv_path = tmp_path / "test.csv"
        fieldnames = list(records[0].keys() if records else [])
        with open(csv_path, "w") as fp:
            writer = csv.DictWriter(fp, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(records)
        return csv_path

    return _csv_file_factory


@pytest.fixture
def csv_file(csv_file_factory):
    return csv_file_factory(RECORDS)


# %% Task 1 tests
# Note that these are not complete and you will be marked on some additional tests
# However, they should definitely be able to infer what many of the missing tests will be just from looking at
#   where these tests are incomplete.


@pytest.mark.parametrize(
    "data_rows,n,expected",
    [
        (RECORDS, 2, True),
        (RECORDS, 3, False),
        ([], 0, True),
        ([], 1, False),
    ],
)
def test_has_n_rows(data_rows, n, expected):
    from data_preprocessing import has_n_rows

    assert has_n_rows(data_rows, n) is expected


@pytest.mark.parametrize(
    "records,column_names,expected",
    [
        (RECORDS, FIELDNAMES, True),
        (RECORDS, FIELDNAMES_PLUS, False),
        ([], FIELDNAMES, False),
        ([], [], True),
    ],
)
def test_has_required_columns(records, column_names, expected, csv_file_factory):
    from data_preprocessing import load_csv, has_required_columns

    csv_file = csv_file_factory(records)
    reader, data_rows = load_csv(csv_file)
    assert has_required_columns(reader, column_names) is expected


@pytest.mark.parametrize(
    "records,column_names,expected",
    [
        (RECORDS_PLUS, FIELDNAMES_PLUS, True),
        (RECORDS_PLUS, FIELDNAMES, False),
        ([], FIELDNAMES_PLUS, True),
        ([], [], True),
    ],
)
def test_has_no_extra_columns(records, column_names, expected, csv_file_factory):
    from data_preprocessing import load_csv, has_no_extra_columns

    csv_file = csv_file_factory(records)
    reader, data_rows = load_csv(csv_file)
    assert has_no_extra_columns(reader, column_names) is expected


@pytest.mark.parametrize(
    "column,expected",
    [
        ([0, 2], 0),
        ([1, None], 0.5),
        (["a", ""], 0.5),
        ([None, ""], 1),
    ],
)
def test_proportion_empty(column, expected):
    from data_preprocessing import proportion_empty

    assert proportion_empty(column) == expected


# This test has the actual proportions your proportion_empty function should return for each column
# Note, you can't just return these values you have to calculate them correctly
# If, on inspection, you are not calculating them you will get 0 for this functionality.
@pytest.mark.parametrize(
    "column_name,expected",
    [
        ("ID", 0),
        ("surname", 0.03842159916926272),
        ("middle.name", 0.632398753894081),
        ("first.name", 0.008307372793354102),
        ("gender", 0.012461059190031152),
        ("systolic_bp", 0),
        ("diastolic_bp", 0),
        ("Age", 0),
        ("weight", 0),
        ("RandomBSL", 0),
        ("DiabetesDiagnosis", 0),
    ],
)
def test_proportion_empty_reference(column_name, expected):
    from data_preprocessing import load_csv, extract_column, proportion_empty

    reader, data_rows = load_csv(DIABETES1_CSV)
    assert proportion_empty(extract_column(data_rows, column_name)) == pytest.approx(
        expected
    )


@pytest.mark.parametrize(
    "row,column_definitions,expected",
    [
        ({}, VALID_ROW_STRUCTURE, False),
        (RECORDS[0], VALID_ROW_STRUCTURE, False),
        (VALID_ROW, VALID_ROW_STRUCTURE, True),
        (VALID_ROW_OPTIONALS, VALID_ROW_STRUCTURE, True),
        (INVALID_ROW_SURNAME, VALID_ROW_STRUCTURE, False),
        (INVALID_ROW_WEIGHT, VALID_ROW_STRUCTURE, False),
    ],
)
def test_is_valid(row, column_definitions, expected):
    from data_preprocessing import is_valid

    assert is_valid(row, column_definitions) is expected


# The child test function is missing an input variable compared to the problem


@pytest.mark.parametrize(
    "data_rows, column_definitions,expected",
    [
        (RECORDS, VALID_ROW_STRUCTURE, 0),
    ],
)
def test_proportion_valid(data_rows, column_definitions, expected):
    from data_preprocessing import proportion_valid

    assert proportion_valid(data_rows, VALID_ROW_STRUCTURE) == expected


# The child test function is missing an input variable compared to the problem


# This tests has the actual proportion your proportion_valid function should return for the entire data set
# Note, you can't just return this value you have to calculate it correctly
# If, on inspection, you are not calculating it you will get 0 for this functionality.
def test_proportion_valid_reference():
    from data_preprocessing import load_csv, proportion_valid

    reader, data_rows = load_csv(DIABETES1_CSV)
    assert proportion_valid(data_rows, VALID_ROW) == pytest.approx(0.5877466251298027)


# %% Task 2 tests


def find_actual_brackets(output):
    for line in output:
        if re.match(r"^Age brackets:", line):
            return line
    return None


def find_actual_counts(output):
    for line in output:
        if re.match(r"^Previously diagnosed:", line):
            return line
    return None


def find_actual_breakdown(output):
    lines = []
    for line in output:
        if re.match(r"^Age bracket [\d\-+]+:", line):
            lines.append(line)
    return lines


def find_actual_ids(output):
    ids = set()
    adding = False
    for line in output:
        if re.match("^Undiagnosed patients over 64:", line):
            adding = True
            continue
        if re.match("=====", line):
            break
        if not adding:
            continue
        ids.add(line)
    return ids


@pytest.mark.parametrize(
    "data_rows,expected_brackets",
    [
        (
            [VALID_ROW],
            "Age brackets: 0-17: 0, 18-24: 0, 25-44: 1, 45-64: 0, 65-84: 0, 85+: 0",
        ),
    ],
)
def test_summarise_data_brackets(data_rows, expected_brackets, capsys):
    from data_preprocessing import summarise_data

    capsys.readouterr()
    summarise_data(data_rows)
    captured = capsys.readouterr()
    output = captured.out.splitlines()
    assert find_actual_brackets(output) == expected_brackets


@pytest.mark.parametrize(
    "data_rows,expected_counts",
    [
        (
            [VALID_ROW],
            "Previously diagnosed: 0 (0.00%), not previously diagnosed: 1 (100.00%)",
        ),
    ],
)
def test_summarise_data_counts(data_rows, expected_counts, capsys):
    from data_preprocessing import summarise_data

    capsys.readouterr()
    summarise_data(data_rows)
    captured = capsys.readouterr()
    output = captured.out.splitlines()
    assert find_actual_counts(output) == expected_counts


@pytest.mark.parametrize(
    "data_rows,expected_breakdown",
    [
        (
            [VALID_ROW],
            [
                "Age bracket 0-17: previously diagnosed: 0, not previously diagnosed: 0",
                "Age bracket 18-24: previously diagnosed: 0, not previously diagnosed: 0",
                "Age bracket 25-44: previously diagnosed: 0, not previously diagnosed: 1",
                "Age bracket 45-64: previously diagnosed: 0, not previously diagnosed: 0",
                "Age bracket 65-84: previously diagnosed: 0, not previously diagnosed: 0",
                "Age bracket 85+: previously diagnosed: 0, not previously diagnosed: 0",
            ],
        ),
    ],
)
def test_summarise_data_breakdown(data_rows, expected_breakdown, capsys):
    from data_preprocessing import summarise_data

    capsys.readouterr()
    summarise_data(data_rows)
    captured = capsys.readouterr()
    output = captured.out.splitlines()
    assert find_actual_breakdown(output) == expected_breakdown


@pytest.mark.parametrize(
    "data_rows,expected_ids",
    [
        ([VALID_ROW], set()),
    ],
)
def test_summarise_data_ids(data_rows, expected_ids, capsys):
    from data_preprocessing import summarise_data

    capsys.readouterr()
    summarise_data(data_rows)
    captured = capsys.readouterr()
    output = captured.out.splitlines()
    assert find_actual_ids(output) == expected_ids


def test_summarise_data_reference(capsys):
    from data_preprocessing import load_csv, summarise_data

    capsys.readouterr()
    reader, data_rows = load_csv(DIABETES1_CSV)
    summarise_data(data_rows)
    captured = capsys.readouterr()
    output = captured.out.splitlines()
    assert (
        find_actual_brackets(output)
        == "Age brackets: 0-17: 29, 18-24: 81, 25-44: 221, 45-64: 217, 65-84: 221, 85+: 141"
    )
    assert (
        find_actual_counts(output)
        == "Previously diagnosed: 59 (6.13%), not previously diagnosed: 851 (88.37%)"
    )
    assert find_actual_breakdown(output) == [
        "Age bracket 0-17: previously diagnosed: 0, not previously diagnosed: 29",
        "Age bracket 18-24: previously diagnosed: 9, not previously diagnosed: 72",
        "Age bracket 25-44: previously diagnosed: 12, not previously diagnosed: 209",
        "Age bracket 45-64: previously diagnosed: 14, not previously diagnosed: 203",
        "Age bracket 65-84: previously diagnosed: 13, not previously diagnosed: 208",
        "Age bracket 85+: previously diagnosed: 11, not previously diagnosed: 130",
    ]
    assert find_actual_ids(output) == {
        "160907",
        "138563",
        "903107",
        "3866",
        "67921",
        "616612",
        "519731",
        "61067",
        "608372",
        "927364",
        "788460",
        "329938",
        "483383",
        "242192",
        "640161",
        "902610",
        "977947",
        "494138",
        "270004",
        "481902",
        "118998",
        "593372",
        "151778",
        "358969",
        "406135",
        "391227",
        "355633",
        "237542",
        "134463",
        "653330",
        "307974",
        "558686",
        "939065",
        "124437",
        "138517",
        "933341",
        "108274",
        "792225",
        "65619",
        "425705",
        "583452",
        "679421",
        "805463",
        "698531",
        "942597",
        "847682",
        "227677",
        "16266",
        "467208",
        "487271",
        "498190",
        "558567",
        "549663",
        "667395",
        "677479",
        "633613",
        "857544",
        "646278",
        "974478",
        "361482",
        "927990",
        "686251",
        "731718",
        "533170",
        "100928",
        "831817",
        "65845",
        "802789",
        "964329",
        "871357",
        "294932",
        "522123",
        "846532",
        "961827",
        "646427",
        "133789",
        "697447",
        "69148",
        "213285",
        "891787",
        "46375",
        "234102",
        "344971",
        "247371",
        "18941",
        "590837",
        "876715",
        "97004",
        "227832",
        "612336",
        "822786",
        "978501",
        "868407",
        "723664",
        "675109",
        "623773",
        "539821",
        "520278",
        "845573",
        "807361",
        "572680",
        "361665",
        "153095",
        "462218",
        "553237",
        "34899",
        "436820",
        "459641",
        "18347",
        "643622",
        "14710",
        "676665",
        "474431",
        "333127",
        "29268",
        "4361",
        "416003",
        "555181",
        "665915",
        "674975",
        "611231",
        "484210",
        "619568",
        "499998",
        "888357",
        "244754",
        "712631",
        "969211",
        "139086",
        "90844",
        "984093",
        "752450",
        "367337",
        "880792",
        "208059",
        "554388",
        "122415",
        "90128",
        "861509",
        "297806",
        "468795",
        "100630",
        "586277",
        "826844",
        "285402",
        "484910",
        "57670",
        "576641",
        "388175",
        "365662",
        "491480",
        "537114",
        "837905",
        "550871",
        "845003",
        "822636",
        "202679",
        "310165",
        "399326",
        "362208",
        "750002",
        "331902",
        "981323",
        "731827",
        "599399",
        "204312",
        "877732",
        "7457",
        "119517",
        "8843",
        "749962",
        "856450",
        "268779",
        "726508",
        "480984",
        "792648",
        "616338",
        "465150",
        "203522",
        "36348",
        "337165",
        "744177",
        "830314",
        "462029",
        "583463",
        "864759",
        "829540",
        "857730",
        "958326",
        "724201",
        "983108",
        "996577",
        "91235",
        "638008",
        "659295",
        "995707",
        "18025",
        "782721",
        "132963",
        "39058",
        "342380",
        "207662",
        "287214",
        "938273",
        "264789",
        "567257",
        "65695",
        "943872",
        "598629",
        "838999",
        "870298",
        "701403",
        "958159",
        "348546",
        "216583",
        "419049",
        "231879",
        "703827",
        "638116",
        "38855",
        "831168",
        "266151",
        "655471",
        "326164",
        "875390",
        "417057",
        "866404",
        "86227",
        "332916",
        "445132",
        "950793",
        "828942",
        "300043",
        "341323",
        "946053",
        "244531",
        "389154",
        "601669",
        "443000",
        "701960",
        "269937",
        "506600",
        "784923",
        "89115",
        "976530",
        "267965",
        "617255",
        "123125",
        "400691",
        "423465",
        "969392",
        "499758",
        "992769",
        "41702",
        "620975",
        "412252",
        "653605",
        "552346",
        "558179",
        "250537",
        "473123",
        "123604",
        "558316",
        "687758",
        "303309",
        "158335",
        "823522",
        "58618",
        "667595",
        "645932",
        "791003",
        "195825",
        "787443",
        "578155",
        "872344",
        "592542",
        "60783",
        "697736",
        "951608",
        "493800",
        "239513",
        "605731",
        "205008",
        "163191",
        "444563",
        "342197",
        "816874",
        "757457",
        "384365",
        "307128",
        "462124",
        "311106",
        "951441",
        "525371",
        "633934",
        "22697",
        "612106",
        "278334",
        "373769",
        "465569",
        "94729",
        "626643",
        "727015",
        "161078",
        "67647",
        "439058",
        "64238",
        "659174",
        "675717",
        "358732",
        "372701",
        "891702",
        "901298",
        "583055",
        "156757",
        "79997",
        "357613",
        "768688",
        "775481",
        "480739",
        "458142",
        "827276",
        "971528",
        "191324",
        "386532",
        "239789",
        "19480",
        "284550",
        "758203",
        "188697",
        "304993",
        "514152",
        "781809",
        "863993",
        "219085",
        "594830",
        "353712",
        "559254",
    }


# %% Task 3
# Test function to calculate MAP
@pytest.mark.parametrize(
    "systolic_bp, diastolic_bp",
    [
        (120, 80),
        (140, 90),
    ],
)
def test_calc_map(systolic_bp, diastolic_bp):
    from data_preprocessing import calc_map

    expected = diastolic_bp + 0.33 * (systolic_bp - diastolic_bp)
    assert calc_map(systolic_bp, diastolic_bp) == pytest.approx(expected)


# Test function to write MAP with given records
@pytest.mark.parametrize(
    "records, expected",
    [
        ([VALID_ROW], [96.62]),  # Example values and expected MAP for VALID_ROW
    ],
)
def test_write_with_map(records, expected):
    from data_preprocessing import load_csv, extract_column, write_with_map

    write_with_map(records)  # Write MAP values to a CSV file
    reader, data_rows = load_csv("preprocessed_data.csv")  # Load the CSV file
    raw_map = extract_column(
        data_rows, "MAP"
    )  # Extract the MAP values from the loaded CSV
    calculated_map = []
    for v in raw_map:
        try:
            calculated_map.append(float(v))
        except ValueError:
            calculated_map.append(None)
    assert calculated_map == pytest.approx(
        expected
    )  # Assert that the calculated MAP values match the expected values


if __name__ == "__main__":
    sys.exit(pytest.main(["-vs", "--tb=no", sys.argv[0]]))
