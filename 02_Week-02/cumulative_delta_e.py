def cumulative_delta_e(delta_e_values):
  """
  Calculate the cumulative sum of ΔE values in a list.
  Parameters:
  delta_e_values(list): a list of ΔE values
  Returns:
  list: A list of cumulative ΔE values
  The function handles:
  -Empty list by returning an empty list.
  -Negative ΔE values can represent errors or darker colours.
  -The function calculates the cumulative sum but does not print it.

  Positive ΔE means brighter colour, negative means darker/error/variability, and zero means no changes.
  """

  # Start with an empty list for the cummulative value
  cumulative_values = []
  colour_changes = []

  # Empty list input
  if len(delta_e_values) != 0:

    # Initialize a variable to keep track of the running total
    running_total = 0

    # Iterate over the list of ΔE values
    for value in delta_e_values:
      # Add the current ΔE to the running total
      running_total += value
      
      # Append the running total to the cummulative sums list
      cumulative_values.append(running_total)
      
      # Determine the colour change based on the cummulative ΔE values
      colour_changes = ['brighter' if v > 0 else 'darker' if v < 0 else 'no change' for v in cumulative_values]

  return cumulative_values, colour_changes


# delta_e_list = [-2, -1, 0, 3, 4, 5]
# cumulative_delta_e(delta_e_list)
