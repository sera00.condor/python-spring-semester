import random

# Mutable Data Type: TREATMENT's data is fixed and does not change
TREATMENT = ['Experimental', 'Standard', 'Placebo']

def assign_groups(list_name):
	'''
		The function should accept a list of names. For each name in the list, it should randomly 
		select a treatment from the TREATMENT list and create a string of the format '<name> is 
		assigned: <treatment group>' (where <name> and <treatment group> are substituted appropriately).

		- Input:
			list_name: Immutable Data Type because the number of elements is unlimited and can be of any length.
	'''

	""" Immutable Data Type: The array has a number of elements that depends on list_name, 
	but list_name does not have a fixed number of elements. """
	output_data = []
	output_data = [data + ' is assigned: ' + random.choice(TREATMENT) for data in list_name]

	return output_data