#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Fri Feb 15 00:36:43 2024

@author: Jane Doe 

This file contains tests for the exercise file assign_groups.py.
"""

import random
import sys
from importlib import reload

import pytest


def test_TREATMENT_defined():
    # Test if the TREATMENT list is correctly defined in the assign_groups module.
    from assign_groups import TREATMENT


@pytest.mark.parametrize('expected', [
    ['Experimental', 'Standard', 'Placebo']
])
def test_TREATMENT(expected):
    # Verify if the TREATMENT list in the assign_groups module matches the expected values.
    from assign_groups import TREATMENT
    assert TREATMENT == expected


def test_assign_groups_defined():
    # Verify if the assign_groups function is correctly defined and accessible.
    from assign_groups import assign_groups


@pytest.mark.parametrize('names, expected', [
        ([], []),
        (['J104'], ['J104 is assigned: Experimental']),
        (['J104', 'D342'], ['J104 is assigned: Experimental', 'D342 is assigned: Placebo']),
])
def test_assign_groups(names, expected):
   # Test assign_groups function with various inputs and expected outputs.
    random.seed(1)
    from assign_groups import assign_groups
    actual = assign_groups(names)
    assert expected == actual


def test_function_does_not_generate_output(capsys):
    # Check if assign_groups function does not output during import and execution.
    # Discard any output generated during import to isolate function behavior.
    from assign_groups import assign_groups
    capsys.readouterr()
    assign_groups([])
    captured = capsys.readouterr()
    assert captured.out == '', 'function should not output anything'
    assert captured.err == '', 'function should not generate any error output'


def test_module_does_not_generate_output(capsys):
    # Verify if assign_groups module does not generate any output during import and execution.
    # Reload the module to ensure it's executed again.
    capsys.readouterr()
    import assign_groups
    reload(assign_groups)
    captured = capsys.readouterr()
    assert captured.out == '', 'module should not output anything'
    assert captured.err == '', 'module should not generate any error output'


if __name__ == "__main__":
    sys.exit(pytest.main(['-vs', '--tb=no']))
